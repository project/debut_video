<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function debut_video_context_default_contexts() {
  $export = array();
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'video-content-type';
  $context->description = '';
  $context->tag = 'Multimedia';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'video' => 'video',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-video-block_1' => array(
          'module' => 'views',
          'delta' => 'video-block_1',
          'region' => 'right',
          'weight' => 0,
        ),
      ),
    ),
    'breadcrumb' => 'video',
    'menu' => 'video',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Multimedia');

  $export['video-content-type'] = $context;
  return $export;
}
