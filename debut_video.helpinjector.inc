<?php

function debut_video_helpinjector_keys() {
  return array(
    'features_admin_form:features:status:debut_video' => array('module' => 'debut_video', 'file' => 'Debut-Video'),
    'video_node_form:field_image:0' => array('module' => 'debut_video', 'file' => 'How-to-take-a-screenshot'),
    'video_node_form:field_video_embed:0:embed' => array('module' => 'debut_video', 'file' => 'Upload-a-video-file-to-an-external-host'),
    'video_node_form:title' => array('module' => 'debut_video', 'file' => 'Posting-a-new-video-file'),
  );

}

